import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Sessions from '../components/Sessions'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/sessions/:category',
      name: 'Sessions',
      component: Sessions
    }
  ]
})
